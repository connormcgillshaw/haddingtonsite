<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'haddington');

/** MySQL database username */
define('DB_USER', 'haddingtonuser');

/** MySQL database password */
define('DB_PASSWORD', '4Qw!wDY5?9z$');

/** MySQL hostname */
define('DB_HOST', '213.171.200.87');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']r%56#2noxu@QUevg*7l3`F1t]j1!65![t]v~u~d~q<s=3@^[sa<|aSHmBjC};S@');
define('SECURE_AUTH_KEY',  'h07h:D5nIRp<bv6[YzU3xP5@5I6Pmg*fM.Oy(T/oY6E{N;Pr&N!v2/]5ydCdARZ}');
define('LOGGED_IN_KEY',    '?Dn=w%g5q,1*CVYm^)3HM?-#7~?Iz1cMaH=69Deq6xeJ<p<WQ(|ZMH^=9A~(ri& ');
define('NONCE_KEY',        '+-=X8WuD`70)eDAmeCf-wf+gii`CgAmh9L$nWym!j(U6{US19+mG{mQ&HN7iCt4@');
define('AUTH_SALT',        'k{1?pN$z^3J|muz7JJgUUILch@R!t10&(&T088|O%M.z@OXR&P/7SGm2P#;m`H}S');
define('SECURE_AUTH_SALT', 'bq5g &:N@)1P<kr&g]8edsYW5G{S=UBR<pZV5bTIk`1,#=|6P@=6up4S5*C1}<@O');
define('LOGGED_IN_SALT',   '`b=N|.e7:U3Ebg7Ig7{vY8OMJ9C.y4X}MBRPH|eGKgj=be{S,7gpfBw_$ngw5m;I');
define('NONCE_SALT',       'I)?qVUX,=(;}&&bc4cJj*8GVK!{fmkJrpg2i( ngS;kgc=k5 .ThZ<ka{Jtx4e13');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
