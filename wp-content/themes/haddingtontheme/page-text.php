 <?php
/*
Template Name: Text Page

*/
?>

<?php get_header(); ?>


<section id="section2">
<div class="container">
  
  	<div class="row">

		<div class="col-md-12" style="margin-top:14em; margin-bottom: 6em;">
		
			<div class="grid cs-style-1">
			
			<?php
				$current_url = home_url(add_query_arg(array(),$wp->request));
				switch ($current_url) {
				case "http://www.thehiddentoun.co.uk/things-to-see-and-do":
					$args = array( 'posts_per_page' => 20, 'category' => 2);
					break;
				case "http://www.thehiddentoun.co.uk/shops-and-markets":
					$args = array( 'posts_per_page' => 20, 'category' => 6);
					break;
				case "http://www.thehiddentoun.co.uk/eating-and-drinking":
					$args = array( 'posts_per_page' => 20, 'category' => 5);
					break;
				default:
					$args = array( 'posts_per_page' => 20, 'category' => 1);
			}
				$myposts = get_posts( $args );
				sort($myposts);
				if(count($myposts)>0){
				}
				
				
				if (count($myposts)>0):
					foreach ( $myposts as $post ) : setup_postdata( $post );
			?>
			<div class="col-md-4" >
                <figure style="margin: 0 0 28px;">
					<div class="q-title"><h3 style="font-size:28px;"><?php the_title(); ?></h3></div>	
                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'article-thumbs' ); ?>		
                    <div class="img-responsive" style="width:450px; height:400px; background-image: url('<?php echo $image[0]; ?>'); background-size: cover; background-position:center"></div>
                    <figcaption>
                        <!-- <h2 class="sarah-txt" style="font-size:28px;"><?php the_title(); ?></h2> -->
                        
                        <figcaption>
                             <p><?php the_excerpt(); ?></p>
                        		<a href="<?php the_permalink(); ?>">View more</a>
                             </figcaption>
                        
                        
                        
					    
                    </figcaption>            
                
                </figure>		
				</div>				
			<?php endforeach; 
			wp_reset_postdata();
			
			else:
			?>
			<p>
				<?php _e('Sorry, there are no posts.'); ?>
			</p>
			<?php endif; ?>
			
			</div>
			</div>
		
		</div>
    
    </div>
    
  </div>

</section>




<?php get_footer(); ?>