<div class="blackbar">

<div class="container">
  
  	<div class="row">  
  <footer>
	<article>
     <div class="col-md-8">
		<div class="pull-left" style="text-align:left;">
			<div class="row">
				<div class="col-md-12">
					<h3 class="greentxt">Haddington Community Development Trust</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-5">
					<span class="col-md-2 foottxt">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
					</span>
					<span class="col-md-9 foottxt">
						X, High Street, Haddington,</br>
						East Lothian EH41 XXX
					</span>
				</div>
				
				<div class="col-md-5">
					<div class="row">
						<span class="col-md-2 foottxt">
							<i class="fa fa-volume-control-phone" aria-hidden="true"></i>
						</span>
						<span class="foottxt" class="col-md-8">
							01620 000 000
						</span>
					</div>
					
					<div class="row">
						<span class="col-md-2 foottxt">
							<i class="fa fa-info-circle" aria-hidden="true"></i>
						</span>
						<span class="foottxt" class="col-md-8">
							info@myhaddington.com
						</span>
					</div>								
				</div>				
			</div>			
		</div>
       </div>
	</article>
	<article>
    	<div class="col-md-4">
 		<div class="pull-right">
			<div class="row">
				<div class="col-md-12" align="left">
					<h3 class="greentxt" style="text-align:left;">Sign-up for offers and updates</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" align="left">
					<h5 class="foottxt" style="text-align:left;">Sign-up for regular email offers and updates</h3>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-11">
						<form id="loginform" class="form-horizontal" role="form">
                                    
                            <div style="margin-bottom: 5px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="Your name">                                        
                                    </div>
                                
                            <div style="margin-bottom: 5px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                        <input id="login-password" type="text" class="form-control" name="password" placeholder="Your email">
                                    </div>
                                    

                                
                           


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <button type="submit" href="#" class="btn btn-success">Sign-up</a>

                                    </div>
                                </div>
                               
                            </form>
					</div>
				</div>				
			</div>			
		</div></div>
	</article>
      </footer>



    </div> 
    
    </div> 
    
    </div>
	<div class="blackbar greenbar">
	
	</div>	
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/toucheffects.js"></script>

    <?php wp_footer(); ?>

  </body>
</html>


  
